---
title: GitOps the ArgoCD Way
type: slide
slideOptions:
    controls: false
    transition: 'none'
---


<style>
.reveal .slide-background.present {
    background-color: #344966;
}
.slide-background-content {
    opacity: 0.5;
    background-image: url("https://i.imgur.com/8rWMxe4.jpg");
}
.slides {
    width: 80% !important;
}
.reveal blockquote {
    width: 90%;
    background: rgb(255, 237, 223, 0.2)
}
.reveal h1 {
    color: #55DDE0;
    font-size: 1.5em;
}
.reveal h2 {
    color: #55DDE0;
    font-size: 1.5em;
}
.reveal a {
    color: #55DDE0;
}
.reveal em{
    color: #279AF1;
    opacity: 0.8;
}

.reveal img {
    margin: 0;

}
.emoji {
    margin-bottom: 0.25em !important;
    width: 45px;
    height: 45px;

}

.reveal .code-wrapper {
    font-size: 15px;
}
</style>
# Kubernetes GitOps the ArgoCD Way

---

## Target Audience
- You have solid Kubernetes Knowledge </br> (kubectl, helm/kustomize)
- You know the basics about Git
- You have seen YAML before :wink:

---

## About me
- Studied Computer Science & Games Engineering
- Responsible for the cluster at my current company
- DevOps Engineer

---

## What is GitOps
- DevOps best practices applied to Infrastructure
- GitOps = IaC + MRs + CI/CD

Note:
    GitOps gibt Ihnen Tools und ein Framework an die Hand, mit dem Sie DevOps-Praktiken wie Zusammenarbeit, CI/CD und Versionskontrolle auf die Infrastrukturautomatisierung und das Deployment von Anwendungen anwenden können.

---


## Key to successful GitOps

- The Git repository has to be the *"Single Source of Truth"*
<!-- .element: class="fragment" data-fragment-index="1" -->
- Infrastructure/Application needs to be declaratively manageable
<!-- .element: class="fragment" data-fragment-index="2" -->
- A tool is used to manage and monitor the resources in the target environment.
<!-- .element: class="fragment" data-fragment-index="3" -->

---

## ArgoCD

> "Argo CD is a declarative, GitOps continuous delivery tool for Kubernetes."

:heavy_check_mark: Manages Application declaratively
<!-- .element: class="fragment" data-fragment-index="1" -->
:heavy_check_mark: Git repository is the Single Source of Truth
<!-- .element: class="fragment" data-fragment-index="2" -->
:heavy_check_mark: Monitors the state of the application
<!-- .element: class="fragment" data-fragment-index="3" -->

---

## ArgoCD :heart: k8s

- Build and designed specifically for Kubernetes
- Specify Kubernetes manifests in several ways
    - Kustomize
    - Helm
    - Jsonnet files
    - YAML/JSON manifests
- Kubernetes controller monitors running applications

---

# ArgoCD Concepts & Definitions


---

## [Architecture](https://argo-cd.readthedocs.io/en/stable/operator-manual/architecture/)

- API Server
- Repository Server
- Application Controller

Note:
    - The API server is a gRPC/REST server which exposes the API consumed by the Web UI, CLI, and CI/CD systems. It has the following responsibilities

    - The repository server is an internal service which maintains a local cache of the Git repository holding the application manifests.

    - The application controller is a Kubernetes controller which continuously monitors running applications and compares the current, live state against the desired target state

---

## Application

```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: demo
  namespace: argo
spec:
  destination:
    namespace: argo
    server: https://kubernetes.default.svc
  project: default
  source:
    path: overlays/staging/
    repoURL: https://gitlab.com/dgiebert/cloudland-2022.git
    targetRevision: HEAD
```

---

## [Project](https://argo-cd.readthedocs.io/en/stable/user-guide/projects/)

- Provides a logical grouping of applications
- Restricts what may be deployed (source)
- Restricts where apps may be deployed to (destination)
- Restricts what kinds of objects can be deployed (type)
- Provides an RBAC mechanism (permission)

---

## Manifest

```yaml
apiVersion: argoproj.io/v1alpha1
kind: AppProject
metadata:
  name: default
  namespace: argo
spec:
  clusterResourceWhitelist:
  - group: '*'
    kind: '*'
  destinations:
  - namespace: '*'
    server: '*'
  sourceRepos:
  - '*'

```

---

## [ApplicationSet](https://argo-cd.readthedocs.io/en/stable/operator-manual/applicationset/)

- Adds multi-cluster and multitenant support
- Improved support for monorepos
- Create apllications using Generators (List, Cluster, Git)

---

## [List Generator](https://argocd-applicationset.readthedocs.io/en/stable/Generators-List/)

```yaml
spec:
  generators:
  - list:
      elements:
      - cluster: dev
        url: https://1.2.3.4
      - cluster: prod
        url: https://9.8.7.6
  template:
    metadata:
      name: 'guestbook-{{cluster}}'
    spec:
      project: default
      source:
        repoURL: https://github.com/argoproj/argo-cd.git
        targetRevision: HEAD
        path: applicationset/examples/list-generator/guestbook/{{cluster}}
      destination:
        server: '{{url}}'
        namespace: guestbook
```

---

## [Git Generator](https://argocd-applicationset.readthedocs.io/en/stable/Generators-Git/)

```yaml
spec:
  generators:
  - git:
      repoURL: https://github.com/argoproj/applicationset.git
      revision: HEAD
      directories:
      - path: examples/git-generator-directory/cluster-addons/*
  template:
    metadata:
      name: '{{path[0]}}'
    spec:
      project: default
      source:
        repoURL: https://github.com/argoproj/applicationset.git
        targetRevision: HEAD
        path: '{{path}}'
      destination:
        server: https://kubernetes.default.svc
        namespace: '{{path.basename}}'
```

Note:
{{path}}: The directory paths within the Git repository that match the path wildcard.
{{path[n]}}: The directory paths within the Git repository that match the path wildcard, split into array elements (n - array index)
{{path.basename}}: For any directory path within the Git repository that matches the path wildcard, the right-most path name is extracted (e.g. /directory/directory2 would produce directory2).
{{path.basenameNormalized}}: This field is the same as path.basename with unsupported characters replaced with - (e.g. a path of /directory/directory_2, and path.basename of directory_2 would produce directory-2 here).

---

## Next Steps
- Create Kustomize Demo Application
- Deploy ArgoCD using Helm
- Create Project and ApplicationSet

---

<!-- .slide: data-background-image="https://c.tenor.com/oFO9mCbbj98AAAAC/rocket-lift-off.gif" -->

## Demo

---

## Sources
- https://about.gitlab.com/topics/gitops/
- https://www.redhat.com/de/topics/devops/what-is-gitops
- https://argo-cd.readthedocs.io/en/stable/