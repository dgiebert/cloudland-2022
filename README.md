# GitOps the ArgoCD Way
This repo contains all the resources that where presented on CloudLand 2022 (Freitag 01.07.2022 | 17:00 - 17:45)

## Structure
```
├── app
│   ├── alembic
│   │   ├── env.py
│   │   ├── script.py.mako
│   │   └── versions
│   │       └── 7c713e28b7e7_new_migration.py
│   ├── alembic.ini
│   ├── Dockerfile
│   ├── main.py
│   ├── models.py
│   ├── requirements.txt
│   └── schema.py
├── argo
│   ├── ApplicationSet.yaml
│   ├── AppProject.yaml
│   ├── ArgoCM.yaml
│   └── helmfile.yaml
├── kustomize
│   ├── base
│   │   ├── deploy.yaml
│   │   ├── ing.yaml
│   │   ├── kustomization.yaml
│   │   ├── ns.yaml
│   │   ├── secret.yaml
│   │   └── svc.yaml
│   └── overlays
│       ├── prod
│       │   ├── deploy.yaml
│       │   └── kustomization.yaml
│       └── staging
│           ├── deploy.yaml
│           └── kustomization.yaml
├── presentation
│   └── source.md
└── README.md

```

## Steps
1. Building the application code
    1. Build and push the init container `docker build --target wait-for-it -t dgiebert/wait-for-it .`
    2. Build and push the init container `docker build -t dgiebert/cloudland .`
2. Install [ArgoCD](https://artifacthub.io/packages/helm/argo/argo-cd): `helmfile --file argo/helmfile.yaml apply`
3. Patch ArgoCD to allow Helm within Kustomize `kubectl apply -f argo/ArgoCM.yaml`
4. Create the Project `kubectl apply -f argo/AppProject.yaml`
5. Create the ApplicationSet `kubectl apply -f argo/ApplicationSet.yaml`

# Sources

- Big Thanks to [Njoku Ifeanyi Gerald]: https://www.educative.io/answers/how-to-use-postgresql-database-in-fastapi
- https://about.gitlab.com/topics/gitops/
- https://www.redhat.com/de/topics/devops/what-is-gitops
- https://argo-cd.readthedocs.io/en/stable/

# License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>